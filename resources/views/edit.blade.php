@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{$qtd}} Usuários Cadastrados</div>

                <div class="card-body">
                  <p>
                    @foreach($dados as $value)
                      {{$value->name}},
                    @endforeach
                  </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Suas Informações</div>

                <div class="card-body">
                  <form action="{{url('/update')}}" method="post">
                    {{ csrf_field() }}
                    <p>Nome: {{$user->name}}</p>
                    <p>Email: {{$user->email}}</p>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Pedido:</label>
                      <textarea class="form-control" aria-describedby="pedidoHelp" id="exampleFormControlTextarea1" rows="3" name="pedido">{{$user->pedido}}</textarea>
                      <small id="pedidoHelp" class="form-text text-muted">Escreva aqui o que você quer ganhar no amigo doce.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </form>
                </div>
            </div>
            <div class="">

            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Amigo Doce</div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
