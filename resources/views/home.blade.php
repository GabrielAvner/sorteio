@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{$qtd}} Usuários Cadastrados</div>

                <div class="card-body">
                  <p>
                    @foreach($dados as $value)
                      {{$value->name}},
                    @endforeach
                  </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Suas Informações</div>

                <div class="card-body">
                  <p>Nome: {{$user->name}}</p>
                  <p>Email: {{$user->email}}</p>
                  <p>Pedido: {{$user->pedido}}</p>
                  <a href="{{url('/edit')}}" class="btn btn-warning">Editar</a>
                </div>
            </div>
            <div class="">

            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Amigo Doce</div>
                <div class="card-body">
                  @if(($user->amigodoce === null))
                  <!-- <center><a href="{{url('/sorteio')}}" class="btn btn-success">Sortear Amigo</a></center> -->
                  <center><a href="{{url('/home')}}" class="btn btn-secondary">Sortear Amigo</a></center>
                  @else
                  <p>Amigo Doce: {{$user->amigo->name}}</p>
                  <p>Pedido:
                    @if($user->amigo->pedido === null)
                     Seu Amigo Doce ainda está escolhendo o pedido. Mande ele ir tomar no cu!
                    @else
                     {{$user->amigo->pedido}}</p>
                    @endif
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
