<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // for ($i=0; $i < 9; $i++) {
        //   $user = new User();
        //   $user->name = $i + 1;
        //   $user->password = $i + 1;
        //   $user->email = $i + 1;
        //   $user->save();
        // }
        $user = Auth::user();
        $dados = User::orderBy('name','ASC')->get();
        $qtd = $dados->count();

        return view('home', compact('user','dados','qtd'));
    }

    public function edit()
    {
      $user = Auth::user();
      $dados = User::orderBy('name','ASC')->get();
      $qtd = $dados->count();

      return view('edit', compact('user','dados','qtd'));
    }

    public function update(Request $request)
    {
      $store = Auth::user();
      $store->fill($request->all());
      $store->save();
      return redirect('/home');
    }

    public function sorteio()
    {
      $user = Auth::user();
      if ($user->amigodoce === null) {
          $dados = User::orderBy('name','ASC')->get();
          $min = 1;
          $max = $dados->count();
          $a = 0;
          while ($a != 1) {
            $amigodoce = rand($min ,$max);
            $esse = User::where('id',$amigodoce)->first();
            if (($esse->sorteado != 1) && ($amigodoce != $user->id)) {
              $a = 1;
            }
          }
          $user->amigodoce = $amigodoce;
          $esse->sorteado = 1;
          $user->save();
          $esse->save();
      }
      return redirect('/home');
    }
}
